# coding: utf-8
from pathlib import Path


_package_path = Path(__file__).parent


# : Путь к файлу базы данных.
DATABASE_FILE = str(
    _package_path.parent.parent / 'photo_gallery.db'
)


# : Префикс для адресов фотографий.
PHOTOS_URL = '/photos'


# : Папка с файлами фотографий
PHOTOS_DIR = str(
    _package_path.parent.parent / 'photos'
)


# : Количество колонок с фотографиями на странице просмотре галлереи.
COLUMN_COUNT = 3
